# Mine-Sweeper
frozen pet project

[Original kata](https://www.codewars.com/kata/57ff9d3b8f7dda23130015fa)

- [x] Main algorithm
- [x] Screenshoter
- [ ] Images splitting into multiple pieces [CVAT](https://github.com/openvinotoolkit/cvat)
- [ ] Image Recognition with a CNN?
